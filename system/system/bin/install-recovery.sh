#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:fa0450571ac6ae250b09f4c061f44e7cac9973c8; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:67108864:b0867e2204f15a22b5d9c7efd613882e935cbae0 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:fa0450571ac6ae250b09f4c061f44e7cac9973c8 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
